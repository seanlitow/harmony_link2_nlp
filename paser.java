import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Collection;
import java.util.List;
import java.io.StringReader;

import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
public class paser {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("This is the commandline mode parser test");
		System.out.println("Please type in the sentence that needs to be parsed and press Enter!");
		String user_input; 
		Scanner scan_console=new Scanner(System.in); 
		LexicalizedParser lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
		
		while(true)
		{
			try{
				user_input=scan_console.nextLine();
				if(user_input.equalsIgnoreCase("exit")) break;
				System.out.println("You have typed: "+user_input);
				demoAPI(lp,user_input);
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}			
			
		}
		System.out.println("You have exited from the program!");
	}
	public static void demoAPI(LexicalizedParser lp, String ls) {

	    // This option shows loading and using an explicit tokenizer
	    String sent2 = ls;
	    TokenizerFactory<CoreLabel> tokenizerFactory =
	      PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
	    List<CoreLabel> rawWords2 =
	      tokenizerFactory.getTokenizer(new StringReader(sent2)).tokenize();
	    Tree parse = lp.apply(rawWords2);

	    TreebankLanguagePack tlp = new PennTreebankLanguagePack();
	    GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
	    GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
	    List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();
	    
	    //System.out.println(tdl);
	    //System.out.println();

	    TreePrint tp = new TreePrint("wordsAndTags,penn,typedDependenciesCollapsed");
	    tp.printTree(parse);
	  }

}
